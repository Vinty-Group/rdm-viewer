with period as (
--     select DATE_FORMAT('2024-06-01 00:00:00','%Y-%m-%d %H:%i:%s') time_start,
--            DATE_FORMAT('2024-06-30 23:59:00','%Y-%m-%d %H:%i:%s') time_end
    select DATE_FORMAT(?1, '%Y-%m-01') time_start,
           DATE_FORMAT((?1, '%Y-%m-01') + INTERVAL 1 MONTH) time_end
)
   ,u as
    (select u.id,u.dep,CONCAT(u.lastname, ' ', u.firstname) name
     from (
              select u.*,'Отдел ОРМ' dep from users u
              where (u.lastname='Тимашов' and u.firstname='Александр')
                 or (u.lastname='Бельский' and u.firstname='Артем')
                 or (u.lastname='Ковалёва' and u.firstname='Анастасия')
                 or (u.lastname='Гончаревич' and u.firstname='Андрей')
                 or (u.lastname='Росляков' and u.firstname='Андрей')
                 or u.lastname in ('Сохин','Захарчик','Дубин','Макарченко')
              union all
              select u.*,'Отдел ОРПИП' dep from users u
              where u.lastname in ('Кастюкевич','Веришко','Горбачик','Сыса')
              union all
              select u.*,'УИИР-1' dep from users u
              where u.login='fedorov_s'
                 or u.lastname in ('Вяткин')
              union all
              select u.*,'Отдел РФС' dep from users u
              where (u.lastname='Ушаков' and u.firstname='Виталий')
                 or (u.lastname='Бойко' and u.firstname='Кирилл')
                 or (u.lastname='Гончар' and u.firstname='Денис Андреевич')
                 or u.lastname in ('Белошеев','Говака','Голузов','Ильина','Мазуров','Пискунович','Сипович','Шартух','Ягшымырадов','Ярохович')
              union all
              select u.*,'УИИР-2' dep from users u
              where (u.lastname='Гончаревич' and u.firstname='Сергей')
                 or (u.lastname='Игнатович' and u.firstname='Илья')
                 or u.lastname in ('Кохович','Стремоус','Ласица')
          ) u
    )
select UUID() AS rn
     ,project_id
     ,project_name
     ,dep
     ,uname
     ,CONCAT('https://rdm.lwo.by/issues/', CAST(task_id AS CHAR)) as ""
     ,(select name from issue_statuses s where s.id=status_id) task_status
     ,work_hours
     ,review_hours
     ,analyse_hours
     ,plan_hours
     ,tr_name
     ,work_type
     ,points
     ,case when work_hours=0 and review_hours=0 and analyse_hours=0 and work_type<>'create' and uid not in (108,159,130,132,433) and plan_hours>0 and not exists (select 1 from time_entries te where te.issue_id = task_id and te.activity_id in (9)) then '*' else '' end no_hours
from (
         select t.uid,t.uname,t.dep
              ,round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (9)),0),1) work_hours
              ,round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (165)),0),1) review_hours
              ,round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (24)),0),1) analyse_hours
              ,round(coalesce(estimated_hours,0),1) plan_hours
              ,task_id,status_id,project_id,p.name project_name,tr.name tr_name,work_type
              ,coalesce(CONVERT(SUBSTR(REGEXP_SUBSTR(t.description,'%@score=(.*?)(?=pts@%)'),9),UNSIGNED),0) points
         from (
                  select distinct
                      i.id task_id,i.description,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep,i.status_id, -- j.created_on dt,
                      case when jd.prop_key='status_id' and jd.old_value='2' then 'work'
                           when jd.prop_key='assigned_to_id' then 'work'
                           when jd.prop_key='status_id' and jd.old_value='37' then 'review'
                           else '?' end work_type
                  from journals j, journal_details jd
                                       left outer join journal_details jd_a on jd_a.journal_id=jd.journal_id and jd_a.property = 'attr' and jd_a.prop_key = 'assigned_to_id',
                       u,issues i,period p
                  where j.created_on between p.time_start and p.time_end -- and j.journalized_id=190917
                    and coalesce(jd_a.old_value,j.user_id) = u.id
                    and jd.journal_id = j.id
                    and jd.property = 'attr'
                    and (
                      (jd.prop_key = 'status_id' and jd.old_value = '2' and jd.value in ('37','13','3')) -- В работе -> Code review,На тестировании,Решена
                          or (jd.prop_key = 'status_id' and jd.old_value = '37' and jd.value in ('2','13','3')) -- Code review -> В работе,На тестировании,Решена
                          or (jd.prop_key = 'assigned_to_id' and jd.old_value = convert(u.id,char)
                          and not exists (select 1 from journal_details jd1 where jd1.journal_id=jd.journal_id and jd1.prop_key='status_id')) -- смена исполнителя без изменения статуса
                          and not exists (select 1 from journals j1, journal_details jd1
                                          where j1.journalized_id=j.journalized_id and jd1.journal_id=j1.id and jd1.prop_key='status_id' and jd1.value='3' and j1.created_on < j.created_on) -- не учитывать перевод, если уже статус Решена
                      )
                    and j.journalized_id=i.id
                  union all
                  select i.id task_id,i.description,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep,i.status_id, -- i.created_on dt,
                         'create' work_type
                  from issues i,u,period p
                  where i.author_id=u.id
                    and i.created_on between p.time_start and p.time_end
              ) t, trackers tr, projects p
         where t.tracker_id=tr.id
           and p.id=t.project_id
         order by project_id
     ) t
order by dep,task_id,case when work_type='create' then 1 when work_type='work' then 2 when work_type='review' then 3 end
;