select
    dep,
    uname,
    sum(work_hours) work_hours,
-- sum(review_hours) review_hours,
    sum(plan_hours) plan_hours,
    sum(num_issues_work) num_issues,
-- sum(num_issues_not_work) num_issues_not_work,
    case when sum(plan_hours)>0 then sum(work_hours)/sum(plan_hours) else 0 end labour
from (
         select project_id,project_name,dep,uname
-- /*
              ,sum(case when plan_hours>0 and work_hours>0 then work_hours else 0 end) work_hours
              ,sum(review_hours) review_hours
              ,sum(case when work_hours>0 and plan_hours>0 then plan_hours else 0 end) plan_hours
              ,count(distinct task_id) num_issues
              ,sum(case when work_hours>0 or review_hours>0 or work_type='create' then 0 else 1 end) num_issues_not_work
              ,sum(case when work_type='work' and work_hours>0 and plan_hours>0 then 1 else 0 end) num_issues_work
              ,sum(case when work_type='review' then 1 else 0 end) num_issues_review
              ,sum(case when work_type='create' then 1 else 0 end) num_issues_create
         -- */
-- ,task_id,work_hours,review_hours,plan_hours,tr_name,work_type
         from (
                  select t.uid,t.uname,t.dep
                       ,case when work_type='work' then round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (9)),0),1) else 0 end work_hours
                       ,case when work_type='review' then round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (165)),0),1) else 0 end review_hours
                       ,case when work_type='work' then round(coalesce(estimated_hours,0),1) else 0 end plan_hours
                       ,task_id,project_id,p.name project_name,tr.name tr_name,work_type
                  from (
                           select distinct
                               i.id task_id,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep, -- j.created_on dt,
                               case when jd.prop_key='status_id' and jd.old_value='2' then 'work'
                                    when jd.prop_key='assigned_to_id' then 'work'
                                    when jd.prop_key='status_id' and jd.old_value='37' then 'review'
                                    else '?' end work_type
                           from journals j, journal_details jd
                                                left outer join journal_details jd_a on jd_a.journal_id=jd.journal_id and jd_a.property = 'attr' and jd_a.prop_key = 'assigned_to_id',
                                (select u.id,u.dep,CONCAT(u.lastname, ' ', u.firstname) name
                                 from (
                                          select u.*,'1' dep from users u
                                          where (u.lastname='Тимашов' and u.firstname='Александр')
                                             or (u.lastname='Бельский' and u.firstname='Артем')
                                             or (u.lastname='Ковалёва' and u.firstname='Анастасия')
                                             or (u.lastname='Гончаревич' and u.firstname='Андрей')
                                             or (u.lastname='Росляков' and u.firstname='Андрей')
                                             or u.lastname in ('Сохин','Захарчик','Дубин','Макарченко')
                                          union all
                                          select u.*,'2' dep from users u
                                          where u.lastname in ('Кастюкевич','Веришко','Горбачик','Сыса')
                                          union all
                                          select u.*,'3' dep from users u
                                          where u.login='fedorov_s'
                                             or u.lastname in ('Вяткин')
                                          union all
                                          select u.*,'4' dep from users u
                                          where (u.lastname='Ушаков' and u.firstname='Виталий')
                                             or (u.lastname='Бойко' and u.firstname='Кирилл')
                                             or (u.lastname='Гончар' and u.firstname='Денис Андреевич')
                                             or u.lastname in ('Белошеев','Говака','Голузов','Ильина','Мазуров','Пискунович','Сипович','Шартух','Ягшымырадов','Ярохович','Лунцевич')
                                          union all
                                          select u.*,'5' dep from users u
                                          where (u.lastname='Гончаревич' and u.firstname='Сергей')
                                             or (u.lastname='Игнатович' and u.firstname='Илья')
                                             or u.lastname in ('Кохович','Стремоус','Ласица')
                                      ) u
                                ) u,
                                issues i
                           where j.created_on between DATE_FORMAT('2024-04-01 00:00:00','%Y-%m-%d %H:%i:%s') and DATE_FORMAT('2024-04-30 23:59:00','%Y-%m-%d %H:%i:%s')-- and j.journalized_id=190917
                             and coalesce(jd_a.old_value,j.user_id) = u.id
                             and jd.journal_id = j.id
                             and jd.property = 'attr'
                             and (
                               (jd.prop_key = 'status_id' and jd.old_value = '2' and jd.value in ('37','13','3')) -- В работе -> Code review,На тестировании,Решена
                                   or (jd.prop_key = 'status_id' and jd.old_value = '37' and jd.value in ('2','13','3')) -- Code review -> В работе,На тестировании,Решена
                                   or (jd.prop_key = 'assigned_to_id' and jd.old_value = convert(u.id,char)
                                   and not exists (select 1 from journal_details jd1 where jd1.journal_id=jd.journal_id and jd1.prop_key='status_id')) -- смена исполнителя без изменения статуса
                                   and not exists (select 1 from journals j1, journal_details jd1
                                                   where j1.journalized_id=j.journalized_id and jd1.journal_id=j1.id and jd1.prop_key='status_id' and jd1.value='3' and j1.created_on < j.created_on) -- не учитывать перевод, если уже статус Решена
                               )
                             and j.journalized_id=i.id
                           union all
                           select i.id task_id,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep, -- i.created_on dt,
                                  'create' work_type
                           from issues i,
                                (select u.id,u.dep,CONCAT(u.lastname, ' ', u.firstname) name
                                 from (
                                          select u.*,'1' dep from users u
                                          where (u.lastname='Тимашов' and u.firstname='Александр')
                                             or (u.lastname='Бельский' and u.firstname='Артем')
                                             or (u.lastname='Ковалёва' and u.firstname='Анастасия')
                                             or (u.lastname='Гончаревич' and u.firstname='Андрей')
                                             or (u.lastname='Росляков' and u.firstname='Андрей')
                                             or u.lastname in ('Сохин','Захарчик','Дубин','Макарченко')
                                          union all
                                          select u.*,'2' dep from users u
                                          where u.lastname in ('Кастюкевич','Веришко','Горбачик','Сыса')
                                          union all
                                          select u.*,'3' dep from users u
                                          where u.login='fedorov_s'
                                             or u.lastname in ('Вяткин')
                                          union all
                                          select u.*,'4' dep from users u
                                          where (u.lastname='Ушаков' and u.firstname='Виталий')
                                             or (u.lastname='Бойко' and u.firstname='Кирилл')
                                             or (u.lastname='Гончар' and u.firstname='Денис Андреевич')
                                             or u.lastname in ('Белошеев','Говака','Голузов','Ильина','Мазуров','Пискунович','Сипович','Шартух','Ягшымырадов','Ярохович','Лунцевич')
                                          union all
                                          select u.*,'5' dep from users u
                                          where (u.lastname='Гончаревич' and u.firstname='Сергей')
                                             or (u.lastname='Игнатович' and u.firstname='Илья')
                                             or u.lastname in ('Кохович','Стремоус','Ласица')
                                      ) u
                                ) u
                           where i.author_id=u.id
                             and i.created_on between DATE_FORMAT('2024-04-01 00:00:00','%Y-%m-%d %H:%i:%s') and DATE_FORMAT('2024-04-30 23:59:00','%Y-%m-%d %H:%i:%s')
                       ) t, trackers tr, projects p
                  where t.tracker_id=tr.id
                    and p.id=t.project_id
                  order by project_id
              ) t
         -- where t.dep=1
--   and uid=1354-- and task_id=195896
-- where task_id=197799
         group by project_id,uname,dep
         order by dep,uname,project_id
-- order by dep,task_id,case when work_type='create' then 1 when work_type='work' then 2 when work_type='review' then 3 end
     ) t
group by dep,uname
order by dep,uname
;