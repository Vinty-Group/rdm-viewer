SELECT
    project_id,
    project_name,
    dep,
    uname,
    CONCAT('https://rdm.lwo.by/issues/', CAST(task_id AS CHAR)) AS "link_to_task",
    sub,
    plan_hours,
    work_hours,
    review_hours,
    tr_name,
    work_type
FROM
    (
        SELECT
            t.uname,
            t.dep
                ,
            round(COALESCE((SELECT sum(hours)
                            FROM time_entries te
                            WHERE te.issue_id = task_id
                              AND te.user_id = t.uid
                              AND te.activity_id IN (9)), 0),
                  1) work_hours
                ,
            round(COALESCE((SELECT sum(hours)
                            FROM time_entries te
                            WHERE te.issue_id = task_id
                              AND te.user_id = t.uid
                              AND te.activity_id IN (165)), 0),
                  1) review_hours
                ,
            round(COALESCE(estimated_hours, 0), 1) plan_hours
                ,
            task_id,
            project_id,
            p.name project_name,
            tr.name tr_name,
            work_type,
            sub
        FROM
            (
                SELECT
                    DISTINCT i.id task_id,
                             i.tracker_id,
                             i.project_id,
                             i.estimated_hours,
                             u.id uid,
                             u.name uname,
                             u.dep,
                             i.subject sub,
                             CASE
                                 WHEN jd.prop_key = 'status_id'
                                     AND jd.old_value = '2' THEN 'work'
                                 WHEN jd.prop_key = 'assigned_to_id' THEN 'work'
                                 WHEN jd.prop_key = 'status_id'
                                     AND jd.old_value = '37' THEN 'review'
                                 ELSE '?'
                                 END work_type
                FROM
                    journals j,
                    journal_details jd
                        LEFT OUTER JOIN journal_details jd_a ON
                        jd_a.journal_id = jd.journal_id
                            AND jd_a.property = 'attr'
                            AND jd_a.prop_key = 'assigned_to_id',
                    (
                        SELECT
                            u.id,
                            u.dep,
                            CONCAT(u.lastname, ' ', u.firstname) name
                        FROM
                            (
                                SELECT
                                    u.*,
                                    'ОРМ' dep
                                FROM
                                    users u
                                WHERE
                                    u.id IN (108, 1354, 1171, 628, 1282, 1146, 989, 614, 94)
                                UNION ALL
                                SELECT
                                    u.*,
                                    'Отдел ОРПИП' dep
                                FROM
                                    users u
                                WHERE
                                    u.id IN (159, 995, 156, 166)
                                UNION ALL
                                SELECT
                                    u.*,
                                    'УИИР-1' dep
                                FROM
                                    users u
                                WHERE
                                    u.id IN (1327, 130)
                                UNION ALL
                                SELECT
                                    u.*,
                                    'Отдел РФС' dep
                                FROM
                                    users u
                                WHERE
                                    u.id IN (433, 1182, 1057, 482, 1160, 1120, 1150, 1243, 954, 1202, 1084, 1129, 988, 444)
                                UNION ALL
                                SELECT
                                    u.*,
                                    'УИИР-2' dep
                                FROM
                                    users u
                                WHERE
                                    u.id IN (132, 1381, 129, 1390, 1391)) u) u,
                    issues i
                WHERE
-- 				YEAR(j.created_on) = YEAR(CURDATE()) AND MONTH(j.created_on) = MONTH(CURDATE())
                    j.created_on >= DATE_FORMAT(NOW(), '%Y-%m-01') AND j.created_on < DATE_FORMAT(NOW(), '%Y-%m-01') + INTERVAL 1 MONTH
						and coalesce(jd_a.old_value,j.user_id) = u.id
  and jd.journal_id = j.id
  and jd.property = 'attr'
  and (
       (jd.prop_key = 'status_id' and jd.old_value = '2' and jd.value in ('37','13','3')) -- В работе -> Code review,На тестировании,Решена
       or (jd.prop_key = 'status_id' and jd.old_value = '37' and jd.value in ('2','13','3')) -- Code review -> В работе,На тестировании,Решена
       or (jd.prop_key = 'assigned_to_id' and jd.old_value = convert(u.id,char)
           and not exists (select 1 from journal_details jd1 where jd1.journal_id=jd.journal_id and jd1.prop_key='status_id')) -- смена исполнителя без изменения статуса
           and not exists (select 1 from journals j1, journal_details jd1
                           where j1.journalized_id=j.journalized_id and jd1.journal_id=j1.id and jd1.prop_key='status_id' and jd1.value='3' and j1.created_on < j.created_on) -- не учитывать перевод, если уже статус Решена
      )
  and j.journalized_id=i.id
                UNION ALL
                SELECT
                    i.id task_id,
                    i.tracker_id,
                    i.project_id,
                    i.estimated_hours,
                    u.id uid,
                    u.name uname,
                    u.dep,
                    i.subject sub,
                    'create' work_type
                FROM
                    issues i,
                    (
                    SELECT
                    u.id,
                    u.dep,
                    CONCAT(u.lastname, ' ', u.firstname) name
                    FROM
                    (
                    SELECT
                    u.*,
                    'ОРМ' dep
                    FROM
                    users u
                    WHERE
                    u.id IN (108, 1354, 1171, 628, 1282, 1146, 989, 614, 94)
                    UNION ALL
                    SELECT
                    u.*,
                    'Отдел ОРПИП' dep
                    FROM
                    users u
                    WHERE
                    u.id IN (159, 995, 156, 166)
                    UNION ALL
                    SELECT
                    u.*,
                    'УИИР-1' dep
                    FROM
                    users u
                    WHERE
                    u.id IN (1327, 130)
                    UNION ALL
                    SELECT
                    u.*,
                    'Отдел РФС' dep
                    FROM
                    users u
                    WHERE
                    u.id IN (433, 1182, 1057, 482, 1160, 1120, 1150, 1243, 954, 1202, 1084, 1129, 988, 444)
                    UNION ALL
                    SELECT
                    u.*,
                    'УИИР-2' dep
                    FROM
                    users u
                    WHERE
                    u.id IN (132, 1381, 129, 1390, 1391)) u) u
                WHERE
-- 							YEAR(i.created_on) = YEAR(CURDATE()) AND MONTH(i.created_on) = MONTH(CURDATE())
                    i.created_on >= DATE_FORMAT(NOW(), '%Y-%m-01') AND i.created_on < DATE_FORMAT(NOW(), '%Y-%m-01') + INTERVAL 1 MONTH
                  AND i.author_id = u.id) t,
            trackers tr,
            projects p
        WHERE
            t.tracker_id = tr.id
          AND p.id = t.project_id
    ) t
ORDER BY
    dep,
    uname,
    CASE
        WHEN work_type = 'create' THEN 1
        WHEN work_type = 'work' THEN 2
        WHEN work_type = 'review' THEN 3
        END








-----------------------------------------------------------------
-- SELECT project_id,
--        project_name,
--        dep,
--        uname,
--        CONCAT('https://rdm.lwo.by/issues/', CAST(task_id AS CHAR)) AS "link_to_task",
--        sub,
--        plan_hours,
--        work_hours,
--        review_hours,
--        tr_name,
--        work_type
-- FROM (SELECT t.uname,
--              t.dep
--               ,
--              round(COALESCE((SELECT sum(hours)
--                              FROM time_entries te
--                              WHERE te.issue_id = task_id
--                                AND te.user_id = t.uid
--                                AND te.activity_id IN (9)), 0),
--                    1)                               work_hours
--               ,
--              round(COALESCE((SELECT sum(hours)
--                              FROM time_entries te
--                              WHERE te.issue_id = task_id
--                                AND te.user_id = t.uid
--                                AND te.activity_id IN (165)), 0),
--                    1)                               review_hours
--               ,
--              round(COALESCE(estimated_hours, 0), 1) plan_hours
--               ,
--              task_id,
--              project_id,
--              p.name                                 project_name,
--              tr.name                                tr_name,
--              work_type,
--              sub
--       FROM (SELECT DISTINCT i.id      task_id,
--                             i.tracker_id,
--                             i.project_id,
--                             i.estimated_hours,
--                             u.id      uid,
--                             u.name    uname,
--                             u.dep,
--                             i.subject sub,
--                             -- j.created_on dt,
--                             CASE
--                                 WHEN jd.prop_key = 'status_id'
--                                     AND jd.old_value = '2' THEN 'work'
--                                 WHEN jd.prop_key = 'assigned_to_id' THEN 'work'
--                                 WHEN jd.prop_key = 'status_id'
--                                     AND jd.old_value = '37' THEN 'review'
--                                 ELSE '?'
--                                 END   work_type
--             FROM journals j,
--                  journal_details jd
--                      LEFT OUTER JOIN journal_details jd_a ON
--                      jd_a.journal_id = jd.journal_id
--                          AND jd_a.property = 'attr'
--                          AND jd_a.prop_key = 'assigned_to_id',
--                  (SELECT u.id,
--                          u.dep,
--                          CONCAT(u.lastname, ' ', u.firstname) name
--                   FROM (SELECT u.*,
--                                'ОРМ' dep
--                         FROM users u
--                         WHERE u.id IN (108, 1354, 1171, 628, 1282, 1146, 989, 614, 94)
--                         UNION ALL
--                         SELECT u.*,
--                                'Отдел ОРПИП' dep
--                         FROM users u
--                         WHERE u.id IN (159, 995, 156, 166)
--                         UNION ALL
--                         SELECT u.*,
--                                'УИИР-1' dep
--                         FROM users u
--                         WHERE u.id IN (1327, 130)
--                         UNION ALL
--                         SELECT u.*,
--                                'Отдел РФС' dep
--                         FROM users u
--                         WHERE u.id IN (433, 1182, 1057, 482, 1160, 1120, 1150, 1243, 954, 1202, 1084, 1129, 988, 444)
--                         UNION ALL
--                         SELECT u.*,
--                                'УИИР-2' dep
--                         FROM users u
--                         WHERE u.id IN (132, 1381, 129, 1390, 1391)) u) u,
--                  issues i
--             WHERE YEAR(j.created_on) = YEAR(CURDATE())
--               AND MONTH(j.created_on) = MONTH(CURDATE())
--               AND COALESCE(jd_a.old_value, j.user_id) = u.id
--               AND jd.journal_id = j.id
--               AND jd.property = 'attr'
--               AND (
--                 (jd.prop_key = 'status_id'
--                     AND jd.old_value = '2'
--                     AND jd.value IN ('37', '13', '3'))
--                     OR (jd.prop_key = 'status_id'
--                     AND jd.old_value = '37'
--                     AND jd.value IN ('2', '13', '3'))
--                     OR (jd.prop_key = 'assigned_to_id'
--                     AND jd.old_value = CONVERT(u.id,
--                                                char)
--                     AND NOT EXISTS (SELECT 1
--                                     FROM journal_details jd1
--                                     WHERE jd1.journal_id = jd.journal_id
--                                       AND jd1.prop_key = 'status_id'))
--                     and not exists (select 1
--                                     from journals j1,
--                                          journal_details jd1
--                                     where j1.journalized_id = j.journalized_id
--                                       and jd1.journal_id = j1.id
--                                       and jd1.prop_key = 'status_id'
--                                       and jd1.value = '3'
--                                       and j1.created_on < j.created_on)
--                 )
--               AND j.journalized_id = i.id
--             UNION ALL
--             SELECT i.id      task_id,
--                    i.tracker_id,
--                    i.project_id,
--                    i.estimated_hours,
--                    u.id      uid,
--                    u.name    uname,
--                    u.dep,
--                    i.subject sub,
--                    'create'  work_type
--             FROM issues i,
--                  (SELECT u.id,
--                          u.dep,
--                          CONCAT(u.lastname, ' ', u.firstname) name
--                   FROM (SELECT u.*,
--                                'ОРМ' dep
--                         FROM users u
--                         WHERE u.id IN (108, 1354, 1171, 628, 1282, 1146, 989, 614, 94)
--                         UNION ALL
--                         SELECT u.*,
--                                'Отдел ОРПИП' dep
--                         FROM users u
--                         WHERE u.id IN (159, 995, 156, 166)
--                         UNION ALL
--                         SELECT u.*,
--                                'УИИР-1' dep
--                         FROM users u
--                         WHERE u.id IN (1327, 130)
--                         UNION ALL
--                         SELECT u.*,
--                                'Отдел РФС' dep
--                         FROM users u
--                         WHERE u.id IN (433, 1182, 1057, 482, 1160, 1120, 1150, 1243, 954, 1202, 1084, 1129, 988, 444)
--                         UNION ALL
--                         SELECT u.*,
--                                'УИИР-2' dep
--                         FROM users u
--                         WHERE u.id IN (132, 1381, 129, 1390, 1391)) u) u
--             WHERE YEAR(i.created_on) = YEAR(CURDATE())
--               AND MONTH(i.created_on) = MONTH(CURDATE())
--               AND i.author_id = u.id) t,
--            trackers tr,
--            projects p
--       WHERE t.tracker_id = tr.id
--         AND p.id = t.project_id
--       ORDER BY project_id) t
-- ORDER BY dep,
--          uname,
--          CASE
--              WHEN work_type = 'create' THEN 1
--              WHEN work_type = 'work' THEN 2
--              WHEN work_type = 'review' THEN 3
--              END
--
