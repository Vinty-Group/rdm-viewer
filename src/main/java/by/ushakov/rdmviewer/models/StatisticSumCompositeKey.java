package by.ushakov.rdmviewer.models;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class StatisticSumCompositeKey implements Serializable {

    private String dep;

    private String uname;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticSumCompositeKey that = (StatisticSumCompositeKey) o;
        return Objects.equals(dep, that.dep) && Objects.equals(uname, that.uname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dep, uname);
    }
}
