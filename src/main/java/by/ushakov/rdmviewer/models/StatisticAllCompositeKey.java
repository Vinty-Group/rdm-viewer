package by.ushakov.rdmviewer.models;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class StatisticAllCompositeKey implements Serializable {
    private String rn;
    private Long project_id;
    private String project_name;
    private String dep;
    private String uname;
    private String linkToTask;
    private float work_hours;
    private float review_hours;
    private float analyse_hours;
    private float plan_hours;
    private String tr_name;
    private String workType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticAllCompositeKey that = (StatisticAllCompositeKey) o;
        return Objects.equals(project_id, that.project_id) && Objects.equals(project_name, that.project_name) && Objects.equals(dep, that.dep) && Objects.equals(uname, that.uname) && Objects.equals(linkToTask, that.linkToTask) && Objects.equals(plan_hours, that.plan_hours) && Objects.equals(work_hours, that.work_hours) && Objects.equals(review_hours, that.review_hours) && Objects.equals(analyse_hours, that.analyse_hours) && Objects.equals(tr_name, that.tr_name) && Objects.equals(workType, that.workType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(project_id, project_name, dep, uname, linkToTask, plan_hours, work_hours, review_hours, analyse_hours, tr_name, workType);
    }
}
