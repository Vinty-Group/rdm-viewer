package by.ushakov.rdmviewer.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@IdClass(StatisticAllCompositeKey.class)
public class StatisticAll {
    @Id
    private String rn;
    private Long project_id;
    private String project_name;
    private String dep;
    private String uname;
    private String linkToTask;
    private String task_status;
    private float work_hours;
    private float review_hours;
    private float analyse_hours;
    private float plan_hours;
    private String tr_name;
    private String workType;
    private Long points;
    private String no_hours;
}
