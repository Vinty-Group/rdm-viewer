package by.ushakov.rdmviewer.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatisticAllDTO {

    private Long project_id;
    private String project_name;
    private String dep;
    private String uname;
    private String linkToTask;
    private String task_status;
    private float work_hours;
    private float review_hours;
    private float analyse_hours;
    private float plan_hours;
    private String tr_name;
    private String workType;
    private float points;
    private float no_hours;
}
