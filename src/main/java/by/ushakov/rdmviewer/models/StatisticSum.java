package by.ushakov.rdmviewer.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@IdClass(StatisticSumCompositeKey.class)
public class StatisticSum {
    @Id
    private String dep;
    @Id
    private String uname;
    private float work_hours;
    private float plan_hours;
    private float num_issues;
    private float all_num_issues;
    private float review_hours;
    private float analyse_hours;
    private float add_work_hours;
    private float all_hours;
    private float num_issues_not_work;
    private float points;
    private float labour;
}
