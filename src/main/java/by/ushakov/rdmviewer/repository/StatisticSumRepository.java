package by.ushakov.rdmviewer.repository;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.models.StatisticSum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface StatisticSumRepository extends JpaRepository<StatisticSum, Long> {

@Query(value = "with period as (\n" +
        "    select DATE_FORMAT(?1, '%Y-%m-01') time_start,\n" +
        "           DATE_FORMAT(?1, '%Y-%m-01') + INTERVAL 1 MONTH time_end \n" +
        ")\n" +
        "   ,u as (\n" +
        "    select u.id,u.dep,CONCAT(u.lastname, ' ', u.firstname) name\n" +
        "    from (\n" +
        "             select u.*,'Отдел ОРМ' dep from users u\n" +
        "             where (u.lastname='Тимашов' and u.firstname='Александр')\n" +
        "                or (u.lastname='Бельский' and u.firstname='Артем')\n" +
        "                or (u.lastname='Ковалёва' and u.firstname='Анастасия')\n" +
        "                or (u.lastname='Гончаревич' and u.firstname='Андрей')\n" +
        "                or (u.lastname='Росляков' and u.firstname='Андрей')\n" +
        "                or u.lastname in ('Сохин','Захарчик','Дубин','Макарченко')\n" +
        "             union all\n" +
        "             select u.*,'Отдел ОРПИП' dep from users u\n" +
        "             where u.lastname in ('Кастюкевич','Веришко','Горбачик','Сыса')\n" +
        "             union all\n" +
        "             select u.*,'УИИР-1' dep from users u\n" +
        "             where u.login='fedorov_s'\n" +
        "                or u.lastname in ('Вяткин')\n" +
        "             union all\n" +
        "             select u.*,'Отдел РФС' dep from users u\n" +
        "             where (u.lastname='Ушаков' and u.firstname='Виталий')\n" +
        "                or (u.lastname='Бойко' and u.firstname='Кирилл')\n" +
        "                or (u.lastname='Гончар' and u.firstname='Денис Андреевич')\n" +
        "                or u.lastname in ('Белошеев','Голузов','Ильина','Мазуров','Пискунович','Сипович','Шартух','Ягшымырадов','Ярохович')\n" +
        "             union all\n" +
        "             select u.*,'УИИР-2' dep from users u\n" +
        "             where (u.lastname='Гончаревич' and u.firstname='Сергей')\n" +
        "                or (u.lastname='Игнатович' and u.firstname='Илья')\n" +
        "                or u.lastname in ('Кохович','Стремоус','Ласица')\n" +
        "         ) u\n" +
        ")\n" +
        "select\n" +
        "    dep,\n" +
        "    uname,\n" +
        "    sum(work_hours) work_hours,\n" +
        "    sum(plan_hours) plan_hours,\n" +
        "    sum(num_issues_work) num_issues, -- число плановых задач с рабочим часами\n" +
        "    sum(num_issues) all_num_issues, -- всего задач\n" +
        "    sum(review_hours) review_hours,\n" +
        "    sum(analyse_hours) analyse_hours,\n" +
        "    sum(add_work_hours) add_work_hours,\n" +
        "    sum(review_hours)+sum(analyse_hours)+sum(add_work_hours)+sum(work_hours) all_hours,\n" +
        "    sum(case when work_hours=0 and review_hours=0 and analyse_hours=0 and add_work_hours=0 and all_plan_hours>0\n" +
        "        and not exists (select 1 from time_entries te where te.issue_id = t.task_id and te.activity_id in (9))\n" +
        "                 then 1 else 0 end) num_issues_not_work, -- число плановых задач без рабочих часов\n" +
        "    sum(points) points,\n" +
        "    case when sum(plan_hours)>0 then sum(plan_hours)/sum(work_hours) else 0 end labour\n" +
        "from (\n" +
        "         select project_id,project_name,dep,uname,task_id\n" +
        "              ,sum(case when plan_hours>0 and work_hours>0 then work_hours else 0 end) work_hours -- рабочие часы в плане\n" +
        "              ,sum(case when plan_hours=0 and work_hours>0 then work_hours else 0 end) add_work_hours -- доп.часы\n" +
        "              ,sum(review_hours) review_hours -- code review часы\n" +
        "              ,sum(distinct round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (24)),0),1)) analyse_hours\n" +
        "              ,sum(case when work_hours>0 and plan_hours>0 then plan_hours else 0 end) plan_hours -- плановые часы (для расчета производительности)\n" +
        "              ,sum(plan_hours) all_plan_hours -- всего плановые часы\n" +
        "              ,count(distinct task_id) num_issues\n" +
        "              ,sum(case when work_type='work' and work_hours>0 and plan_hours>0 then 1 else 0 end) num_issues_work\n" +
        "              ,sum(case when work_type='review' then 1 else 0 end) num_issues_review\n" +
        "              ,sum(case when work_type='create' then 1 else 0 end) num_issues_create\n" +
        "              ,sum(case when work_hours>0 then points else 0 end) points\n" +
        "         from (\n" +
        "                  select t.uid,t.uname,t.dep\n" +
        "                       ,case when work_type='work' then round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (9)),0),1) else 0 end work_hours\n" +
        "                       ,case when work_type='review' then round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (165)),0),1) else 0 end review_hours\n" +
        "                       ,case when work_type='work' then round(coalesce(estimated_hours,0),1) else 0 end plan_hours\n" +
        "                       ,task_id,project_id,p.name project_name,tr.name tr_name,work_type\n" +
        "                       ,coalesce(CONVERT(SUBSTR(REGEXP_SUBSTR(t.description,'%@score=(.*?)(?=pts@%)'),9),UNSIGNED),0) points\n" +
        "                  from (\n" +
        "                           select distinct\n" +
        "                               i.id task_id,i.description,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep, -- j.created_on dt,\n" +
        "                               case when jd.prop_key='status_id' and jd.old_value='2' then 'work'\n" +
        "                                    when jd.prop_key='assigned_to_id' then 'work'\n" +
        "                                    when jd.prop_key='status_id' and jd.old_value='37' then 'review'\n" +
        "                                    else '?' end work_type\n" +
        "                           from journals j, journal_details jd\n" +
        "                                                left outer join journal_details jd_a on jd_a.journal_id=jd.journal_id and jd_a.property = 'attr' and jd_a.prop_key = 'assigned_to_id',\n" +
        "                                u,issues i,period p\n" +
        "                           where j.created_on between p.time_start and p.time_end -- and j.journalized_id=190917\n" +
        "                             and coalesce(jd_a.old_value,j.user_id) = u.id\n" +
        "                             and jd.journal_id = j.id\n" +
        "                             and jd.property = 'attr'\n" +
        "                             and (\n" +
        "                               (jd.prop_key = 'status_id' and jd.old_value = '2' and jd.value in ('37','13','3')) -- В работе -> Code review,На тестировании,Решена\n" +
        "                                   or (jd.prop_key = 'status_id' and jd.old_value = '37' and jd.value in ('2','13','3')) -- Code review -> В работе,На тестировании,Решена\n" +
        "                                   or (jd.prop_key = 'assigned_to_id' and jd.old_value = convert(u.id,char)\n" +
        "                                   and not exists (select 1 from journal_details jd1 where jd1.journal_id=jd.journal_id and jd1.prop_key='status_id')) -- смена исполнителя без изменения статуса\n" +
        "                                   and not exists (select 1 from journals j1, journal_details jd1\n" +
        "                                                   where j1.journalized_id=j.journalized_id and jd1.journal_id=j1.id and jd1.prop_key='status_id' and jd1.value='3' and j1.created_on < j.created_on) -- не учитывать перевод, если уже статус Решена\n" +
        "                               )\n" +
        "                             and j.journalized_id=i.id\n" +
        "                           union\n" +
        "                           select i.id task_id,i.description,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep, -- i.created_on dt,\n" +
        "                                  'create' work_type\n" +
        "                           from issues i,u,period p\n" +
        "                           where i.author_id=u.id\n" +
        "                             and i.created_on between p.time_start and p.time_end\n" +
        "                       ) t, trackers tr, projects p\n" +
        "                  where t.tracker_id=tr.id\n" +
        "                    and p.id=t.project_id\n" +
        "--  and task_id=196984\n" +
        "                  order by project_id\n" +
        "              ) t\n" +
        "         group by project_id,uname,dep,task_id\n" +
        "         order by dep,uname,project_id\n" +
        "     ) t\n" +
        "group by dep,uname\n" +
        "order by dep,uname\n" +
        ";", nativeQuery = true)
    List<StatisticSum> findSumStatistic(LocalDate localDate);
}
