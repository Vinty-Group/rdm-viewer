package by.ushakov.rdmviewer.repository;

import by.ushakov.rdmviewer.models.StatisticAll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface StatisticAllDateRepository extends JpaRepository<StatisticAll, Long> {

//    @Query(value =
//            "select UUID() AS                                                  rn\n" +
//                    "     , project_id\n" +
//                    "     , project_name\n" +
//                    "     , dep\n" +
//                    "     , uname\n" +
//                    "    /*\n" +
//                    "     ,sum(work_hours) work_hours\n" +
//                    "     ,sum(review_hours) review_hours\n" +
//                    "     ,sum(plan_hours) plan_hours\n" +
//                    "     ,count(distinct task_id) num_issues\n" +
//                    "     ,sum(case when work_type='work' then 1 else 0 end) num_issues_work\n" +
//                    "     ,sum(case when work_type='review' then 1 else 0 end) num_issues_review\n" +
//                    "     ,sum(case when work_type='create' then 1 else 0 end) num_issues_create\n" +
//                    "   -- */\n" +
//                    "     , CONCAT('https://rdm.lwo.by/issues/', CAST(task_id AS CHAR)) AS \"link_to_task\"\n" +
//                    "     , (select name from issue_statuses s where s.id = status_id) task_status\n" +
//                    "     , work_hours\n" +
//                    "     , review_hours\n" +
//                    "     , analyse_hours\n" +
//                    "     , plan_hours\n" +
//                    "     , tr_name\n" +
//                    "     , work_type\n" +
//                    "from (select t.uid\n" +
//                    "           , t.uname\n" +
//                    "           , t.dep\n" +
//                    "           , round(coalesce((select sum(hours)\n" +
//                    "                             from time_entries te\n" +
//                    "                             where te.issue_id = task_id and te.user_id = t.uid and te.activity_id in (9)), 0),\n" +
//                    "                   1)                               work_hours\n" +
//                    "           , round(coalesce((select sum(hours)\n" +
//                    "                             from time_entries te\n" +
//                    "                             where te.issue_id = task_id and te.user_id = t.uid and te.activity_id in (165)), 0),\n" +
//                    "                   1)                               review_hours\n" +
//                    "           , round(coalesce((select sum(hours)\n" +
//                    "                             from time_entries te\n" +
//                    "                             where te.issue_id = task_id and te.user_id = t.uid and te.activity_id in (24)), 0),\n" +
//                    "                   1)                               analyse_hours\n" +
//                    "           , round(coalesce(estimated_hours, 0), 1) plan_hours\n" +
//                    "           , task_id\n" +
//                    "           , status_id\n" +
//                    "           , project_id\n" +
//                    "           , p.name                                 project_name\n" +
//                    "           , tr.name                                tr_name\n" +
//                    "           , work_type\n" +
//                    "      from (select distinct i.id             task_id,\n" +
//                    "                            i.tracker_id,\n" +
//                    "                            i.project_id,\n" +
//                    "                            i.estimated_hours,\n" +
//                    "                            u.id              uid,\n" +
//                    "                            u.name           uname,\n" +
//                    "                            u.dep,\n" +
//                    "                            i.status_id, -- j.created_on dt,\n" +
//                    "                            case\n" +
//                    "                                when jd.prop_key = 'status_id' and jd.old_value = '2' then 'work'\n" +
//                    "                                when jd.prop_key = 'assigned_to_id' then 'work'\n" +
//                    "                                when jd.prop_key = 'status_id' and jd.old_value = '37' then 'review'\n" +
//                    "                                else '?' end work_type\n" +
//                    "            from journals j,\n" +
//                    "                 journal_details jd\n" +
//                    "                     left outer join journal_details jd_a\n" +
//                    "                                     on jd_a.journal_id = jd.journal_id and jd_a.property = 'attr' and\n" +
//                    "                                        jd_a.prop_key = 'assigned_to_id',\n" +
//                    "                 (select u.id, u.dep, CONCAT(u.lastname, ' ', u.firstname) name\n" +
//                    "                  from (select u.*, 'Отдел ОРМ' dep\n" +
//                    "                        from users u\n" +
//                    "                        where (u.lastname = 'Тимашов' and u.firstname = 'Александр')\n" +
//                    "                           or (u.lastname = 'Бельский' and u.firstname = 'Артем')\n" +
//                    "                           or (u.lastname = 'Ковалёва' and u.firstname = 'Анастасия')\n" +
//                    "                           or (u.lastname = 'Гончаревич' and u.firstname = 'Андрей')\n" +
//                    "                           or (u.lastname = 'Росляков' and u.firstname = 'Андрей')\n" +
//                    "                           or u.lastname in ('Сохин', 'Захарчик', 'Дубин', 'Макарченко')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'Отдел ОРПИП' dep\n" +
//                    "                        from users u\n" +
//                    "                        where u.lastname in ('Кастюкевич', 'Веришко', 'Горбачик', 'Сыса')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'УИИР-1' dep\n" +
//                    "                        from users u\n" +
//                    "                        where u.login = 'fedorov_s'\n" +
//                    "                           or u.lastname in ('Вяткин')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'Отдел РФС' dep\n" +
//                    "                        from users u\n" +
//                    "                        where (u.lastname = 'Ушаков' and u.firstname = 'Виталий')\n" +
//                    "                           or (u.lastname = 'Бойко' and u.firstname = 'Кирилл')\n" +
//                    "                           or (u.lastname = 'Гончар' and u.firstname = 'Денис Андреевич')\n" +
//                    "                           or u.lastname in\n" +
//                    "                              ('Белошеев', 'Говака', 'Голузов', 'Ильина', 'Мазуров', 'Пискунович', 'Сипович', 'Шартух',\n" +
//                    "                               'Ягшымырадов', 'Ярохович', 'Лунцевич')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'УИИР-2' dep\n" +
//                    "                        from users u\n" +
//                    "                        where (u.lastname = 'Гончаревич' and u.firstname = 'Сергей')\n" +
//                    "                           or (u.lastname = 'Игнатович' and u.firstname = 'Илья')\n" +
//                    "                           or u.lastname in ('Кохович', 'Стремоус', 'Ласица')) u) u,\n" +
//                    "                 issues i\n" +
//                    "            where j.created_on >= DATE_FORMAT(?1, '%Y-%m-01') AND j.created_on < DATE_FORMAT(?1, '%Y-%m-01') + INTERVAL 1 MONTH-- and j.journalized_id=190917\n" +
//                    "              and coalesce(jd_a.old_value, j.user_id) = u.id\n" +
//                    "              and jd.journal_id = j.id\n" +
//                    "              and jd.property = 'attr'\n" +
//                    "              and (\n" +
//                    "                (jd.prop_key = 'status_id' and jd.old_value = '2' and\n" +
//                    "                 jd.value in ('37', '13', '3')) -- В работе -> Code review,На тестировании,Решена\n" +
//                    "                    or (jd.prop_key = 'status_id' and jd.old_value = '37' and\n" +
//                    "                        jd.value in ('2', '13', '3')) -- Code review -> В работе,На тестировании,Решена\n" +
//                    "                    or (jd.prop_key = 'assigned_to_id' and jd.old_value = convert(u.id, char)\n" +
//                    "                    and not exists (select 1\n" +
//                    "                                    from journal_details jd1\n" +
//                    "                                    where jd1.journal_id = jd.journal_id\n" +
//                    "                                      and jd1.prop_key = 'status_id')) -- смена исполнителя без изменения статуса\n" +
//                    "                    and not exists (select 1\n" +
//                    "                                    from journals j1,\n" +
//                    "                                         journal_details jd1\n" +
//                    "                                    where j1.journalized_id = j.journalized_id\n" +
//                    "                                      and jd1.journal_id = j1.id\n" +
//                    "                                      and jd1.prop_key = 'status_id'\n" +
//                    "                                      and jd1.value = '3'\n" +
//                    "                                      and j1.created_on < j.created_on) -- не учитывать перевод, если уже статус Решена\n" +
//                    "                )\n" +
//                    "              and j.journalized_id = i.id\n" +
//                    "            union all\n" +
//                    "            select i.id     task_id,\n" +
//                    "                   i.tracker_id,\n" +
//                    "                   i.project_id,\n" +
//                    "                   i.estimated_hours,\n" +
//                    "                   u.id     uid,\n" +
//                    "                   u.name   uname,\n" +
//                    "                   u.dep,\n" +
//                    "                   i.status_id, -- i.created_on dt,\n" +
//                    "                   'create' work_type\n" +
//                    "            from issues i,\n" +
//                    "                 (select u.id, u.dep, CONCAT(u.lastname, ' ', u.firstname) name\n" +
//                    "                  from (select u.*, 'Отдел ОРМ' dep\n" +
//                    "                        from users u\n" +
//                    "                        where (u.lastname = 'Тимашов' and u.firstname = 'Александр')\n" +
//                    "                           or (u.lastname = 'Бельский' and u.firstname = 'Артем')\n" +
//                    "                           or (u.lastname = 'Ковалёва' and u.firstname = 'Анастасия')\n" +
//                    "                           or (u.lastname = 'Гончаревич' and u.firstname = 'Андрей')\n" +
//                    "                           or (u.lastname = 'Росляков' and u.firstname = 'Андрей')\n" +
//                    "                           or u.lastname in ('Сохин', 'Захарчик', 'Дубин', 'Макарченко')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'Отдел ОРПИП' dep\n" +
//                    "                        from users u\n" +
//                    "                        where u.lastname in ('Кастюкевич', 'Веришко', 'Горбачик', 'Сыса')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'УИИР-1' dep\n" +
//                    "                        from users u\n" +
//                    "                        where u.login = 'fedorov_s'\n" +
//                    "                           or u.lastname in ('Вяткин')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'Отдел РФС' dep\n" +
//                    "                        from users u\n" +
//                    "                        where (u.lastname = 'Ушаков' and u.firstname = 'Виталий')\n" +
//                    "                           or (u.lastname = 'Бойко' and u.firstname = 'Кирилл')\n" +
//                    "                           or (u.lastname = 'Гончар' and u.firstname = 'Денис Андреевич')\n" +
//                    "                           or u.lastname in\n" +
//                    "                              ('Белошеев', 'Говака', 'Голузов', 'Ильина', 'Мазуров', 'Пискунович', 'Сипович', 'Шартух',\n" +
//                    "                               'Ягшымырадов', 'Ярохович', 'Лунцевич')\n" +
//                    "                        union all\n" +
//                    "                        select u.*, 'УИИР-2' dep\n" +
//                    "                        from users u\n" +
//                    "                        where (u.lastname = 'Гончаревич' and u.firstname = 'Сергей')\n" +
//                    "                           or (u.lastname = 'Игнатович' and u.firstname = 'Илья')\n" +
//                    "                           or u.lastname in ('Кохович', 'Стремоус', 'Ласица')) u) u\n" +
//                    "            where i.author_id = u.id\n" +
//                    "              and i.created_on >= DATE_FORMAT(?1, '%Y-%m-01') AND i.created_on < DATE_FORMAT(?1, '%Y-%m-01') + INTERVAL 1 MONTH) t,\n" +
//                    "           trackers tr,\n" +
//                    "           projects p\n" +
//                    "      where t.tracker_id = tr.id\n" +
//                    "        and p.id = t.project_id\n" +
//                    "      order by project_id) t\n" +
//                    "-- where t.dep=5\n" +
//                    "--   and uid=1354 and review_hours>0\n" +
//                    "-- where work_hours=0 and review_hours=0 and work_type<>'create' and uid not in (108,159,130,132,433) and plan_hours>0 and not exists (select 1 from time_entries te where te.issue_id = task_id and te.activity_id in (9))\n" +
//                    "-- where task_id=197799\n" +
//                    "-- group by project_id,uname,dep\n" +
//                    "-- order by dep,uname,project_id\n" +
//                    "order by dep, task_id,\n" +
//                    "         case when work_type = 'create' then 1 when work_type = 'work' then 2 when work_type = 'review' then 3 end\n" +
//                    ";", nativeQuery = true)
//    List<StatisticAll> findAllDateStatistic(LocalDate localDate);
@Query(value =
        "with period as (\n" +
//                "     select DATE_FORMAT('2024-06-01','%Y-%m-%d') time_start,\n" +
//                "            DATE_FORMAT('2024-06-30','%Y-%m-%d') time_end\n" +
                "    select DATE_FORMAT(?1, '%Y-%m-01') time_start,\n" +
                "           DATE_FORMAT(?1, '%Y-%m-01') + INTERVAL 1 MONTH time_end\n" +
                ")\n" +
                "   ,u as\n" +
                "    (select u.id,u.dep,CONCAT(u.lastname, ' ', u.firstname) name\n" +
                "     from (\n" +
                "              select u.*,'Отдел ОРМ' dep from users u\n" +
                "              where (u.lastname='Тимашов' and u.firstname='Александр')\n" +
                "                 or (u.lastname='Бельский' and u.firstname='Артем')\n" +
                "                 or (u.lastname='Ковалёва' and u.firstname='Анастасия')\n" +
                "                 or (u.lastname='Гончаревич' and u.firstname='Андрей')\n" +
                "                 or (u.lastname='Росляков' and u.firstname='Андрей')\n" +
                "                 or u.lastname in ('Сохин','Захарчик','Дубин','Макарченко')\n" +
                "              union all\n" +
                "              select u.*,'Отдел ОРПИП' dep from users u\n" +
                "              where u.lastname in ('Кастюкевич','Веришко','Горбачик','Сыса')\n" +
                "              union all\n" +
                "              select u.*,'УИИР-1' dep from users u\n" +
                "              where u.login='fedorov_s'\n" +
                "                 or u.lastname in ('Вяткин')\n" +
                "              union all\n" +
                "              select u.*,'Отдел РФС' dep from users u\n" +
                "              where (u.lastname='Ушаков' and u.firstname='Виталий')\n" +
                "                 or (u.lastname='Бойко' and u.firstname='Кирилл')\n" +
                "                 or (u.lastname='Гончар' and u.firstname='Денис Андреевич')\n" +
                "                 or u.lastname in ('Белошеев','Говака','Голузов','Ильина','Мазуров','Пискунович','Сипович','Шартух','Ягшымырадов','Ярохович')\n" +
                "              union all\n" +
                "              select u.*,'УИИР-2' dep from users u\n" +
                "              where (u.lastname='Гончаревич' and u.firstname='Сергей')\n" +
                "                 or (u.lastname='Игнатович' and u.firstname='Илья')\n" +
                "                 or u.lastname in ('Кохович','Стремоус','Ласица')\n" +
                "          ) u\n" +
                "    )\n" +
                "select UUID() AS rn \n" +
                "     ,project_id\n" +
                "     ,project_name\n" +
                "     ,dep\n" +
                "     ,uname\n" +
                "     ,CONCAT('https://rdm.lwo.by/issues/', CAST(task_id AS CHAR)) AS \"link_to_task\" \n" +
                "     ,(select name from issue_statuses s where s.id=status_id) task_status\n" +
                "     ,work_hours\n" +
                "     ,review_hours\n" +
                "     ,analyse_hours\n" +
                "     ,plan_hours\n" +
                "     ,tr_name\n" +
                "     ,work_type\n" +
                "     ,points\n" +
                "     ,case when work_hours=0 and review_hours=0 and analyse_hours=0 and work_type<>'create' and uid not in (108,159,130,132,433) and plan_hours>0 and not exists (select 1 from time_entries te where te.issue_id = task_id and te.activity_id in (9)) then '*' else '' end no_hours\n" +
                "from (\n" +
                "         select t.uid,t.uname,t.dep\n" +
                "              ,round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (9)),0),1) work_hours\n" +
                "              ,round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (165)),0),1) review_hours\n" +
                "              ,round(coalesce((select sum(hours) from time_entries te where te.issue_id = task_id and te.user_id=t.uid and te.activity_id in (24)),0),1) analyse_hours\n" +
                "              ,round(coalesce(estimated_hours,0),1) plan_hours\n" +
                "              ,task_id,status_id,project_id,p.name project_name,tr.name tr_name,work_type\n" +
                "              ,coalesce(CONVERT(SUBSTR(REGEXP_SUBSTR(t.description,'%@score=(.*?)(?=pts@%)'),9),UNSIGNED),0) points\n" +
                "         from (\n" +
                "                  select distinct\n" +
                "                      i.id task_id,i.description,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep,i.status_id, -- j.created_on dt,\n" +
                "                      case when jd.prop_key='status_id' and jd.old_value='2' then 'work'\n" +
                "                           when jd.prop_key='assigned_to_id' then 'work'\n" +
                "                           when jd.prop_key='status_id' and jd.old_value='37' then 'review'\n" +
                "                           else '?' end work_type\n" +
                "                  from journals j, journal_details jd\n" +
                "                                       left outer join journal_details jd_a on jd_a.journal_id=jd.journal_id and jd_a.property = 'attr' and jd_a.prop_key = 'assigned_to_id',\n" +
                "                       u,issues i,period p\n" +
                "                  where j.created_on between p.time_start and p.time_end -- and j.journalized_id=190917\n" +
                "                    and coalesce(jd_a.old_value,j.user_id) = u.id\n" +
                "                    and jd.journal_id = j.id\n" +
                "                    and jd.property = 'attr'\n" +
                "                    and (\n" +
                "                      (jd.prop_key = 'status_id' and jd.old_value = '2' and jd.value in ('37','13','3')) -- В работе -> Code review,На тестировании,Решена\n" +
                "                          or (jd.prop_key = 'status_id' and jd.old_value = '37' and jd.value in ('2','13','3')) -- Code review -> В работе,На тестировании,Решена\n" +
                "                          or (jd.prop_key = 'assigned_to_id' and jd.old_value = convert(u.id,char)\n" +
                "                          and not exists (select 1 from journal_details jd1 where jd1.journal_id=jd.journal_id and jd1.prop_key='status_id')) -- смена исполнителя без изменения статуса\n" +
                "                          and not exists (select 1 from journals j1, journal_details jd1\n" +
                "                                          where j1.journalized_id=j.journalized_id and jd1.journal_id=j1.id and jd1.prop_key='status_id' and jd1.value='3' and j1.created_on < j.created_on) -- не учитывать перевод, если уже статус Решена\n" +
                "                      )\n" +
                "                    and j.journalized_id=i.id\n" +
                "                  union all\n" +
                "                  select i.id task_id,i.description,i.tracker_id,i.project_id,i.estimated_hours,u.id uid,u.name uname,u.dep,i.status_id, -- i.created_on dt,\n" +
                "                         'create' work_type\n" +
                "                  from issues i,u,period p\n" +
                "                  where i.author_id=u.id\n" +
                "                    and i.created_on between p.time_start and p.time_end\n" +
                "              ) t, trackers tr, projects p\n" +
                "         where t.tracker_id=tr.id\n" +
                "           and p.id=t.project_id\n" +
                "         order by project_id\n" +
                "     ) t\n" +
                "order by dep,task_id,case when work_type='create' then 1 when work_type='work' then 2 when work_type='review' then 3 end\n" +
                ";", nativeQuery = true)
List<StatisticAll> findAllDateStatistic(LocalDate localDate);


}


