package by.ushakov.rdmviewer.repository;

import by.ushakov.rdmviewer.models.StatisticAll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatisticAllRepository extends JpaRepository<StatisticAll, Long> {

        @Query(value =
                "SELECT UUID() AS rn, \n" +
                "    project_id,\n" +
                "    project_name,\n" +
                "    dep,\n" +
                "    uname,\n" +
                "    CONCAT('https://rdm.lwo.by/issues/', CAST(task_id AS CHAR)) AS \"link_to_task\",\n" +
                "    sub,\n" +
                "    plan_hours,\n" +
                "    work_hours,\n" +
                "    review_hours,\n" +
                "    tr_name,\n" +
                "    work_type\n" +
                "FROM\n" +
                "    (\n" +
                "        SELECT\n" +
                "            t.uname,\n" +
                "            t.dep\n" +
                "                ,\n" +
                "            round(COALESCE((SELECT sum(hours)\n" +
                "                            FROM time_entries te\n" +
                "                            WHERE te.issue_id = task_id\n" +
                "                              AND te.user_id = t.uid\n" +
                "                              AND te.activity_id IN (9)), 0),\n" +
                "                  1) work_hours\n" +
                "                ,\n" +
                "            round(COALESCE((SELECT sum(hours)\n" +
                "                            FROM time_entries te\n" +
                "                            WHERE te.issue_id = task_id\n" +
                "                              AND te.user_id = t.uid\n" +
                "                              AND te.activity_id IN (165)), 0),\n" +
                "                  1) review_hours\n" +
                "                ,\n" +
                "            round(COALESCE(estimated_hours, 0), 1) plan_hours\n" +
                "                ,\n" +
                "            task_id,\n" +
                "            project_id,\n" +
                "            p.name project_name,\n" +
                "            tr.name tr_name,\n" +
                "            work_type,\n" +
                "            sub\n" +
                "        FROM\n" +
                "            (\n" +
                "                SELECT\n" +
                "                    DISTINCT i.id task_id,\n" +
                "                             i.tracker_id,\n" +
                "                             i.project_id,\n" +
                "                             i.estimated_hours,\n" +
                "                             u.id uid,\n" +
                "                             u.name uname,\n" +
                "                             u.dep,\n" +
                "                             i.subject sub,\n" +
                "                             CASE\n" +
                "                                 WHEN jd.prop_key = 'status_id'\n" +
                "                                     AND jd.old_value = '2' THEN 'work'\n" +
                "                                 WHEN jd.prop_key = 'assigned_to_id' THEN 'work'\n" +
                "                                 WHEN jd.prop_key = 'status_id'\n" +
                "                                     AND jd.old_value = '37' THEN 'review'\n" +
                "                                 ELSE '?'\n" +
                "                                 END work_type\n" +
                "                FROM\n" +
                "                    journals j,\n" +
                "                    journal_details jd\n" +
                "                        LEFT OUTER JOIN journal_details jd_a ON\n" +
                "                        jd_a.journal_id = jd.journal_id\n" +
                "                            AND jd_a.property = 'attr'\n" +
                "                            AND jd_a.prop_key = 'assigned_to_id',\n" +
                "                    (\n" +
                "                        SELECT\n" +
                "                            u.id,\n" +
                "                            u.dep,\n" +
                "                            CONCAT(u.lastname, ' ', u.firstname) name\n" +
                "                        FROM\n" +
                "                            (\n" +
                "                                SELECT\n" +
                "                                    u.*,\n" +
                "                                    'Отдел ОРМ' dep\n" +
                "                                FROM\n" +
                "                                    users u\n" +
                "                                WHERE\n" +
                "                                    u.id IN (108, 1354, 1171, 628, 1282, 1146, 989, 614, 94)\n" +
                "                                UNION ALL\n" +
                "                                SELECT\n" +
                "                                    u.*,\n" +
                "                                    'Отдел ОРПИП' dep\n" +
                "                                FROM\n" +
                "                                    users u\n" +
                "                                WHERE\n" +
                "                                    u.id IN (159, 995, 156, 166)\n" +
                "                                UNION ALL\n" +
                "                                SELECT\n" +
                "                                    u.*,\n" +
                "                                    'УИИР-1' dep\n" +
                "                                FROM\n" +
                "                                    users u\n" +
                "                                WHERE\n" +
                "                                    u.id IN (1327, 130)\n" +
                "                                UNION ALL\n" +
                "                                SELECT\n" +
                "                                    u.*,\n" +
                "                                    'Отдел РФС' dep\n" +
                "                                FROM\n" +
                "                                    users u\n" +
                "                                WHERE\n" +
                "                                    u.id IN (433, 1182, 1057, 482, 1160, 1120, 1150, 1243, 954, 1202, 1084, 1129, 988, 444)\n" +
                "                                UNION ALL\n" +
                "                                SELECT\n" +
                "                                    u.*,\n" +
                "                                    'УИИР-2' dep\n" +
                "                                FROM\n" +
                "                                    users u\n" +
                "                                WHERE\n" +
                "                                    u.id IN (132, 1381, 129, 1390, 1391)) u) u,\n" +
                "                    issues i\n" +
                "                WHERE\n" +
                "-- \t\t\t\tYEAR(j.created_on) = YEAR(CURDATE()) AND MONTH(j.created_on) = MONTH(CURDATE()) \n" +
                "                    j.created_on >= DATE_FORMAT(NOW(), '%Y-%m-01') AND j.created_on < DATE_FORMAT(NOW(), '%Y-%m-01') + INTERVAL 1 MONTH\n" +
                "\t\t\t\t\t\tAND COALESCE(jd_a.old_value, j.user_id) = u.id\n" +
                "\t\t\t\t\t\t\tAND jd.journal_id = j.id\n" +
                "\t\t\t\t\t\t\tAND jd.property = 'attr'\n" +
                "\t\t\t\t\t\t\tAND (\n" +
                "                (jd.prop_key = 'status_id'\n" +
                "\t\t\t\t\t\t\t\tAND jd.old_value = '2'\n" +
                "\t\t\t\t\t\t\t\tAND jd.value IN ('37', '13', '3'))\n" +
                "\t\t\t\t\t\t\t\tOR (jd.prop_key = 'status_id'\n" +
                "\t\t\t\t\t\t\t\t\tAND jd.old_value = '37'\n" +
                "\t\t\t\t\t\t\t\t\tAND jd.value IN ('2', '13', '3'))\n" +
                "\t\t\t\t\t\t\t\t\tOR (jd.prop_key = 'assigned_to_id'\n" +
                "\t\t\t\t\t\t\t\t\t\tAND jd.old_value = CONVERT(u.id,\n" +
                "\t\t\t\t\t\t\t\t\t\tchar)\n" +
                "\t\t\t\t\t\t\t\t\t\t\tAND NOT EXISTS (\n" +
                "\t\t\t\t\t\t\t\t\t\t\tSELECT\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t1\n" +
                "\t\t\t\t\t\t\t\t\t\t\tFROM\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\tjournal_details jd1\n" +
                "\t\t\t\t\t\t\t\t\t\t\tWHERE\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\tjd1.journal_id = jd.journal_id\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\tAND jd1.prop_key = 'status_id'))\n" +
                "\t\t\t\t\t\t\t\t\tand not exists (\n" +
                "\t\t\t\t\t\t\t\t\tselect\n" +
                "\t\t\t\t\t\t\t\t\t\t1\n" +
                "\t\t\t\t\t\t\t\t\tfrom\n" +
                "\t\t\t\t\t\t\t\t\t\tjournals j1,\n" +
                "\t\t\t\t\t\t\t\t\t\tjournal_details jd1\n" +
                "\t\t\t\t\t\t\t\t\twhere\n" +
                "\t\t\t\t\t\t\t\t\t\tj1.journalized_id = j.journalized_id\n" +
                "\t\t\t\t\t\t\t\t\t\tand jd1.journal_id = j1.id\n" +
                "\t\t\t\t\t\t\t\t\t\tand jd1.prop_key = 'status_id'\n" +
                "\t\t\t\t\t\t\t\t\t\tand jd1.value = '3'\n" +
                "\t\t\t\t\t\t\t\t\t\tand j1.created_on < j.created_on)\n" +
                "                )\n" +
                "\t\t\t\t\t\t\tAND j.journalized_id = i.id\n" +
                "                UNION ALL\n" +
                "                SELECT\n" +
                "                    i.id task_id,\n" +
                "                    i.tracker_id,\n" +
                "                    i.project_id,\n" +
                "                    i.estimated_hours,\n" +
                "                    u.id uid,\n" +
                "                    u.name uname,\n" +
                "                    u.dep,\n" +
                "                    i.subject sub,\n" +
                "                    'create' work_type\n" +
                "                FROM\n" +
                "                    issues i,\n" +
                "                    (\n" +
                "                    SELECT\n" +
                "                    u.id,\n" +
                "                    u.dep,\n" +
                "                    CONCAT(u.lastname, ' ', u.firstname) name\n" +
                "                    FROM\n" +
                "                    (\n" +
                "                    SELECT\n" +
                "                    u.*,\n" +
                "                    'Отдел ОРМ' dep\n" +
                "                    FROM\n" +
                "                    users u\n" +
                "                    WHERE\n" +
                "                    u.id IN (108, 1354, 1171, 628, 1282, 1146, 989, 614, 94)\n" +
                "                    UNION ALL\n" +
                "                    SELECT\n" +
                "                    u.*,\n" +
                "                    'Отдел ОРПИП' dep\n" +
                "                    FROM\n" +
                "                    users u\n" +
                "                    WHERE\n" +
                "                    u.id IN (159, 995, 156, 166)\n" +
                "                    UNION ALL\n" +
                "                    SELECT\n" +
                "                    u.*,\n" +
                "                    'УИИР-1' dep\n" +
                "                    FROM\n" +
                "                    users u\n" +
                "                    WHERE\n" +
                "                    u.id IN (1327, 130)\n" +
                "                    UNION ALL\n" +
                "                    SELECT\n" +
                "                    u.*,\n" +
                "                    'Отдел РФС' dep\n" +
                "                    FROM\n" +
                "                    users u\n" +
                "                    WHERE\n" +
                "                    u.id IN (433, 1182, 1057, 482, 1160, 1120, 1150, 1243, 954, 1202, 1084, 1129, 988, 444)\n" +
                "                    UNION ALL\n" +
                "                    SELECT\n" +
                "                    u.*,\n" +
                "                    'УИИР-2' dep\n" +
                "                    FROM\n" +
                "                    users u\n" +
                "                    WHERE\n" +
                "                    u.id IN (132, 1381, 129, 1390, 1391)) u) u\n" +
                "                WHERE\n" +
                "-- \t\t\t\t\t\t\tYEAR(i.created_on) = YEAR(CURDATE()) AND MONTH(i.created_on) = MONTH(CURDATE())\n" +
                "                    i.created_on >= DATE_FORMAT(NOW(), '%Y-%m-01') AND i.created_on < DATE_FORMAT(NOW(), '%Y-%m-01') + INTERVAL 1 MONTH\n" +
                "                  AND i.author_id = u.id) t,\n" +
                "            trackers tr,\n" +
                "            projects p\n" +
                "        WHERE\n" +
                "            t.tracker_id = tr.id\n" +
                "          AND p.id = t.project_id\n" +
                "    ) t\n" +
                "ORDER BY\n" +
                "    dep,\n" +
                "    uname,\n" +
                "    CASE\n" +
                "        WHEN work_type = 'create' THEN 1\n" +
                "        WHEN work_type = 'work' THEN 2\n" +
                "        WHEN work_type = 'review' THEN 3\n" +
                "        END", nativeQuery = true)
    List<StatisticAll> findAllStatistic();
}
