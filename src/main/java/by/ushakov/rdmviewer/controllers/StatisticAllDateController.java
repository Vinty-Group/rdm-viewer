package by.ushakov.rdmviewer.controllers;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.services.StatisticAllDateService;
import by.ushakov.rdmviewer.utils.DateClass;
import by.ushakov.rdmviewer.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Validated
@RestController
//@RequestMapping("/vinty")
@RequiredArgsConstructor
public class StatisticAllDateController {

    private final StatisticAllDateService statisticAllDateService;

    @GetMapping(value = "/getStatisticAllDate/{localDate}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<StatisticAll> getStatisticAllDate(@PathVariable @NotNull String localDate) {
        DateClass dateClass = new DateClass(localDate);
        ResponseList<StatisticAll> responseList = new ResponseList<StatisticAll>();
        responseList.setContent(statisticAllDateService.findAllDateStatistic(dateClass.getLocalDate()));
        responseList.setRespCode("200");
        System.out.println("Отработка запроса БД - " + new Date());
        return responseList;
    }
}
