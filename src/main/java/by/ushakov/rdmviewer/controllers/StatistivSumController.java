package by.ushakov.rdmviewer.controllers;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.models.StatisticSum;
import by.ushakov.rdmviewer.services.impl.StatisticAllService;
import by.ushakov.rdmviewer.services.impl.StatisticSumService;
import by.ushakov.rdmviewer.utils.DateClass;
import by.ushakov.rdmviewer.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@Validated
@RestController
//@RequestMapping("/vinty")
@RequiredArgsConstructor
public class StatistivSumController {

    private final StatisticSumService statisticSumService;

    @GetMapping(value = "/getStatisticSum/{localDate}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<StatisticSum> getStatisticSum(@PathVariable @NotNull String localDate) {
        DateClass dateClass = new DateClass(localDate);
        ResponseList<StatisticSum> responseList = new ResponseList<StatisticSum>();
        responseList.setContent(statisticSumService.findSumStatistic(dateClass.getLocalDate()));
        responseList.setRespCode("200");
        return responseList;
    }
}
