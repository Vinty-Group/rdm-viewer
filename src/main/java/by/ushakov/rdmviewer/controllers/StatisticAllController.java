package by.ushakov.rdmviewer.controllers;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.services.impl.StatisticAllService;
import by.ushakov.rdmviewer.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Validated
@RestController
//@RequestMapping("/vinty")
@RequiredArgsConstructor
public class StatisticAllController {

    private final StatisticAllService statisticAllService;

    @GetMapping(value = "/getStatisticAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<StatisticAll> getStatisticAll() {
        ResponseList<StatisticAll> responseList = new ResponseList<StatisticAll>();
        responseList.setContent(statisticAllService.findAllStatistic());
        responseList.setRespCode("200");
        System.out.println(LocalDateTime.now() + " - Выполнен запрос в БД");
        return responseList;
    }
}
