package by.ushakov.rdmviewer;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Log
@PropertySource(value = "classpath:application.properties")
@ConfigurationPropertiesScan
@EnableJpaRepositories
public class RestApiPrcApplication {

      public static void main(String[] args) {
        SpringApplication.run(RestApiPrcApplication.class, args);

        log.info("-------------------------------------------------------------------------------");
        log.info("----------------------REDMINE-VIEWER SERVICE STARTED!--------------------------");
        log.info("-------------------------------------------------------------------------------");
    }
}
