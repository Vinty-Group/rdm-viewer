package by.ushakov.rdmviewer.services;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.models.StatisticSum;

import java.time.LocalDate;
import java.util.List;

public interface IStatisticSumService {

    List<StatisticSum> findSumStatistic(LocalDate localDate);

}
