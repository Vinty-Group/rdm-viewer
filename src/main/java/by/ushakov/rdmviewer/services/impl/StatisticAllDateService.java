package by.ushakov.rdmviewer.services.impl;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.repository.StatisticAllDateRepository;
import by.ushakov.rdmviewer.utils.DateClass;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatisticAllDateService implements by.ushakov.rdmviewer.services.StatisticAllDateService {

    private StatisticAllDateRepository statisticAllDateRepository;

    @Autowired
    public StatisticAllDateService(StatisticAllDateRepository statisticAllDateRepository) {
        this.statisticAllDateRepository = statisticAllDateRepository;
    }

    @Transactional
    public List<StatisticAll> findAllDateStatistic(LocalDate localDate) {
        Optional<List<StatisticAll>> allStatistic = Optional.ofNullable(statisticAllDateRepository.findAllDateStatistic(localDate));
        return allStatistic.orElse(new ArrayList<>());
    }
}
