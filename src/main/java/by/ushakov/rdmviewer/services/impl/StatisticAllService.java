package by.ushakov.rdmviewer.services.impl;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.repository.StatisticAllRepository;
import by.ushakov.rdmviewer.services.IStatisticAllService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatisticAllService implements IStatisticAllService {

    private StatisticAllRepository statisticAllRepository;

    @Autowired
    public StatisticAllService(StatisticAllRepository statisticAllRepository) {
        this.statisticAllRepository = statisticAllRepository;
    }

    @Transactional
    public List<StatisticAll> findAllStatistic() {
        Optional<List<StatisticAll>> allStatistic = Optional.ofNullable(statisticAllRepository.findAllStatistic());
        return allStatistic.orElse(new ArrayList<>());
    }
}
