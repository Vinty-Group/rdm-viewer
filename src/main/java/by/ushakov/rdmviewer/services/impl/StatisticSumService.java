package by.ushakov.rdmviewer.services.impl;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.models.StatisticSum;
import by.ushakov.rdmviewer.repository.StatisticSumRepository;
import by.ushakov.rdmviewer.services.IStatisticSumService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatisticSumService implements IStatisticSumService {

    private StatisticSumRepository statisticSumRepository;

    @Autowired
    public StatisticSumService(StatisticSumRepository statisticSumRepository) {
        this.statisticSumRepository = statisticSumRepository;
    }

    @Transactional
    public List<StatisticSum> findSumStatistic(LocalDate localDate) {
        Optional<List<StatisticSum>> sumStatistic = Optional.ofNullable(statisticSumRepository.findSumStatistic(localDate));
        return sumStatistic.orElse(new ArrayList<>());
    }
}
