package by.ushakov.rdmviewer.services;

import by.ushakov.rdmviewer.models.StatisticAll;
import by.ushakov.rdmviewer.utils.DateClass;

import java.time.LocalDate;
import java.util.List;

public interface StatisticAllDateService {

    List<StatisticAll> findAllDateStatistic(LocalDate localDate);

}
