package by.ushakov.rdmviewer.services;

import by.ushakov.rdmviewer.models.StatisticAll;

import java.util.List;

public interface IStatisticAllService {

    List<StatisticAll> findAllStatistic();

}
