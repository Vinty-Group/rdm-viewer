package by.ushakov.rdmviewer.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class JsonHelper {

    /**
     * Check if JSON is valid
     * */
    public boolean isValid(@NonNull String json) {
        try {
            new JSONObject(json);
        } catch (JSONException e) {
            try {
                new JSONArray(json);
            } catch (JSONException ne) {
                return false;
            }
        }
        return true;
    }


}
