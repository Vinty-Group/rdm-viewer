package by.ushakov.rdmviewer.utils;

import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
public class DateClass {

    private LocalDate localDate;

    public DateClass(String dateString) {
        // Парсим строку в LocalDate
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        this.localDate = LocalDate.parse(dateString, formatter);
    }

}
