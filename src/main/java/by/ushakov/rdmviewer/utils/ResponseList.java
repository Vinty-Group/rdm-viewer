package by.ushakov.rdmviewer.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
@Data
@NoArgsConstructor
@Component
@AllArgsConstructor
@Builder
public class ResponseList<T> {
    private String respCode;
    private String respMess;
    private List<T> content;

}


