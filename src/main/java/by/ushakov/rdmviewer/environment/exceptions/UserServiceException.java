package by.ushakov.rdmviewer.environment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.stream.Collectors;

public class UserServiceException extends ResponseStatusException {
    private static final HttpStatus DEFAULT_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    private HttpStatus currentStatus = DEFAULT_STATUS;

    public UserServiceException(String message, Throwable cause) {
        super(DEFAULT_STATUS, message, cause);
    }

    public UserServiceException(HttpStatus status, String message) {
        super(status, message);
        this.currentStatus = status;
    }

    public UserServiceException(HttpStatus status, String ... messages) {
        super(status, Arrays.stream(messages)
                .filter(m -> m != null && !m.isEmpty())
                .collect(Collectors.joining(". ")));
        this.currentStatus = status;
    }

    public UserServiceException(HttpStatus status, String message, Throwable cause) {
        super(status, message, cause);
        this.currentStatus = status;
    }

    HttpStatus getHttpStatus() {
        return currentStatus;
    }
}
