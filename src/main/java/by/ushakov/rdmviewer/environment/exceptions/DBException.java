package by.ushakov.rdmviewer.environment.exceptions;

import lombok.Getter;

@Getter
public class DBException extends RuntimeException{

    private final String code;

    public DBException(String code, String message) {
        super(message);
        this.code = code;
    }
}
