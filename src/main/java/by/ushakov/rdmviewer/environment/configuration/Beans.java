package by.ushakov.rdmviewer.environment.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.result.method.RequestMappingInfo;
import org.springframework.web.reactive.result.method.RequestMappingInfoHandlerMapping;

import java.lang.reflect.Method;

@Configuration
public class Beans {
    @Bean
    public RequestMappingInfoHandlerMapping requestMappingInfoHandlerMapping() {
        return new RequestMappingInfoHandlerMapping() {
            @Override
            protected boolean isHandler(Class<?> aClass) {
                return false;
            }

            @Override
            protected RequestMappingInfo getMappingForMethod(Method method, Class<?> aClass) {
                return null;
            }
        };
    }
}
